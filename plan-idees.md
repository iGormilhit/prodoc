# Idées et plan

## Mots clés en vrac

   * machine à écrire
      * la mise en page en mode ASCII
	  * calcul du nombre de ligne
	  * insertion de tableaux
   * éditeur de texte en mode ASCII (!= tabulation)
   * éditeur de texte avec balise
   * traitement de texte utilisé comme une machine à écrire
      * l'espacement fait avec des retours à la ligne
	  * chaque mise en forme fait "à la main"
   * traitement de texte utilisé avec des styles et des modèles
   * LaTeX
   * markdown et pandoc
      * → PDF (LaTeX simplifé), voir Authorea
      * → ODT
	  * → ePub
	  * docbook
	  * dokuwiki
   * Authorea
   * git, suivi de version
   * local / web
   * collaboratif
   * libre (liberté des utilisateurs)
   * binaire, format textuel

### Pour des pistes de recherche

   * [word processor](http://en.wikipedia.org/wiki/Word_processor)
   * [document modelling](http://en.wikipedia.org/wiki/Document_modelling)
   * CMS, Content Managment System
   * [Digital Typesetting](http://en.wikipedia.org/wiki/Typesetting#Digital_era)

Je cherche à mieux nommer l'objet de mon intérêt, sans grand succès. *Document creation* ? Édition de document ? Il me manque vraiment des mots. Ou alors, il faut que je définisse plus clairement ce que j'entends par &laquo; production documentaire &raquo;.


## Idées

Confession d'un *typonazi* raté
