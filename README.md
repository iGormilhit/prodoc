# Production documentaire

Ce dépôt est celui d'un projet de texte sur la *production documentaire*, ou la *production de documents*, axé sur l'utilisation d'outils alternatifs aux traitements de texte. On pense bien entendu à *LaTeX*, mais également au fameux couple *Markdown / Pandoc*. La production documentaire n'est plus seulement celle de documents figés, prêts à l'impression, mais aussi une *publication web*, que ce soit pour produire des sites statiques (*Jekyll*, *Pelican*, etc.) ou dynamique avec les *CMS*.

Je profite de ce projet pour expérimenter *GitBook*, mais en [local](https://github.com/GitbookIO/gitbook), au moyen de mon éditeur préféré, *vim* (je n'ai pas été convaincu par l'*[editor](https://github.com/GitbookIO/editor)* de *GitBook*).

Ce qui ne devrait pas m'empêcher de convertir des parties pour les publier sur mon [blog](https://id-libre.org/blogigor), en attendant la fin du projet.
