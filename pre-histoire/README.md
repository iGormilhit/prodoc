# La pré histoire

Les méthodes de production de document assistées par ordinateur, domaine que je nomme &mdash; improprement, mais je n'ai rien trouvé d'autre &mdash; *production documentaire*, me passionnent depuis de nombreuses années. Cet intérêt date forcément d'une époque reculées, afin de respecter l'orthodoxie de notre culture empesée, de l'exploration de l'écriture manuscrite, puis de l'apprentissage de la machine à écrire. Tout repose sans doute sur l'illusion d'un document fini, proche du livre. Sans avoir encore conscience de l'existence de l'art de composer une page, un chapitre, un livre, dont le but est de servir la lisibilité, qui fait bien entendu partie de l'accessibilité, en toute logique.

Les moines copistes et les imprimeurs s'intéressaient à l'ergonomie...

En découvrant les possibilités offertes par l'informatique, comme la plupart, j'ai d'abord appris à utiliser les traitements de texte, c'est-à-dire *Microsoft Word*, puis *OpenOffice.org* et *LibreOffice*.

Il m'a fallu tout de même un certain temps avant que je réalise pleinement les avantages de l'utilisation des styles et des modèles de documents. Je me souviens du sentiment d'étrangeté complète lors d'un cours de bureautique vers 1992-3, au collège, alors qu'un barbu &mdash; était-il libriste ? &mdash; cherchait à nous faire comprendre qu'un traitement de texte n'était pas une machine à écrire... Je découvrais la possibilité des sélections intelligentes pour supprimer, copier, voire couper ; la différence entre un retour de paragraphe et un simple retour à la ligne ; des raccourcis clavier, alors que l'utilisation de la souris, sur un poste PC du moins, n'était pas si récente, selon mon expérience.

Mais la structuration ! On entre assez lentement dans l'univers des documents structurés, du moins ça a été mon cas. Pourtant, l'imprimé est en général structuré, et le lecteur apprenait, et apprends encore, à s'appuyer sur celle-ci pour explorer l'espace de l'ouvrage : numéros de page, table des matières, titres de parties, de chapitres, etc. Mais associée à la puissance de l'ordinateur, cette structure offre des possibilités nouvelles, époustouflantes. Pour autant qu'elle soit accessible à la machine.

Or, la machine a besoin d'un coup de main pour nous venir en aide : aide-toi et la machine...

La première fonction que l'on découvre généralement, c'est le fait de générer automatiquement des tables des matières, des tables d'illustrations, voire des index. Puis, on constate que l'utilisation des style permet de donner une cohérence aux documents, et d'améliorer leur maintenance. Il ne reste plus qu'un pas pour se mettre, *avant* de travailler, à réaliser un modèle de document. Pourtant, on reste quand même dans le bricolage, l'approximation typographique. En partie parce que l'on est enfermé dans le modèle *WYSIWYG*, cette illusion de maîtrise.
